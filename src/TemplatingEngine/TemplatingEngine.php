<?php

namespace TemplatingEngine;

class TemplatingEngine {

    private $_replacements;
    private $_code;
    private $_arguments;
    private $_storageEngine;

    public function __construct()
    {
        $this->_replacements = array();
        $this->_code = '';
        $this->_arguments = array();
        $this->_storageEngine = null;
    }

    public function setStorageEngine($storageEngine)
    {
        $this->_storageEngine = $storageEngine;
    }

    public function process($code, $arguments = array())
    {
        $this->_code = $code;
        $this->_arguments = $arguments;
        $this->_addReplacementForIncludes();
        $this->_addReplacementForIfStatements();
        $this->_addReplacementForPHPTags();
        $this->_addReplacementForEchoStatements();
        $this->_makeReplacements();

        return $this->_evalCode();
    }

    private function _addReplacementForIncludes()
    {
        preg_match_all('#{% include \((.*)\) %}#', $this->_code, $matches);

        if (is_array($matches[1]) && sizeof($matches[1]) > 0) {
            foreach ($matches[1] as $i => $match) {
                $include = $matches[0][$i];
                $file = str_replace('\'', '', $matches[1][$i]);

                if ($this->_storageEngine) {
                    $html = $this->_storageEngine->get(VIEWS_FOLDER . $file);
                    $templatingEngine = new self();
                    $templatingEngine->setStorageEngine($this->_storageEngine);
                    $this->_code = preg_replace('#' . preg_quote($include) . '#', $templatingEngine->process($html, $this->_arguments), $this->_code);
                }
            }
        }
    }

    private function _addReplacementForIfStatements()
    {
        $this->_replacements[]= array('/{% if \((.*)\) %}/', '<?php if ($1) : ?>');
        $this->_replacements[]= array('/{% elseif(.*) %}/', '<?php elseif ($1) : ?>');
        $this->_replacements[]= array('/{% else %}/', '<?php else : ?>');
        $this->_replacements[]= array('/{% endif %}/', '<?php endif; ?>');
    }

    private function _addReplacementForPHPTags()
    {
        $this->_replacements[]= array('/{% /', '<?php ');
        $this->_replacements[]= array('/ %}/', ' ?>');
    }

    private function _addReplacementForEchoStatements()
    {
        $this->_replacements[]= array('/\{\{ (.*?)\|raw \}\}/', '<?php echo $1; ?>');
        $this->_replacements[]= array('/\{\{ (.*?)\ \}\}/', '<?php echo htmlspecialchars($1, ENT_QUOTES, \'UTF-8\'); ?>');
    }

    private function _makeReplacements()
    {
        $patterns = $replacements = array();
        foreach ($this->_replacements as $replacement) {
            $patterns[]= $replacement[0];
            $replacements[]= $replacement[1];
        }
        $this->_code = preg_replace($patterns, $replacements, $this->_code);
    }

    private function _evalCode()
    {
        if (is_array($this->_arguments) && sizeof($this->_arguments) > 0) {
            foreach ($this->_arguments as $key => $value) {
                $$key = $value;
            }
        }

        ob_start();
        eval(' ?>' . $this->_code . '<?php ');
        $this->_code = ob_get_contents();
        ob_end_clean();

        return $this->_code;
    }

}