<?php

class TemplatingEngineTest extends PHPUnit_Framework_TestCase
{

    public function testConstruct()
    {
        $templatingEngine = new \TemplatingEngine\TemplatingEngine();

        $this->assertInstanceOf('\TemplatingEngine\TemplatingEngine', $templatingEngine);
    }

    public function testProcess()
    {
        $templatingEngine = new \TemplatingEngine\TemplatingEngine();
        $content = "test";

        $this->assertEquals('test', $templatingEngine->process($content));
    }

    public function testProcessIfStatement()
    {
        $templatingEngine = new \TemplatingEngine\TemplatingEngine();
        $content = "{% if true %}\ntest{% endif %}";

        $this->assertEquals('test', $templatingEngine->process($content));
    }

    public function testProcessIfStatement2()
    {
        $templatingEngine = new \TemplatingEngine\TemplatingEngine();
        $content = "{% if 2 == 1 %}\nyes{% else %}\nno{% endif %}";

        $this->assertEquals('no', $templatingEngine->process($content));
    }

    public function testEcho()
    {
        $templatingEngine = new \TemplatingEngine\TemplatingEngine();
        $content = "{{ 'test' }}";

        $this->assertEquals('test', $templatingEngine->process($content));
    }

    public function testForLoop()
    {
        $templatingEngine = new \TemplatingEngine\TemplatingEngine();
        $content = \XHTML\HTMLParser::getFileContent($this->_getFilesFolder() . 'template2.html');

        $this->assertEquals("1-2-3-", $templatingEngine->process($content));
    }

    public function testForeachLoop()
    {
        $templatingEngine = new \TemplatingEngine\TemplatingEngine();
        $content = \XHTML\HTMLParser::getFileContent($this->_getFilesFolder() . 'template3.html');

        $this->assertEquals("Jack-John-", $templatingEngine->process($content, array('items' => array(0 => array('name' => 'Jack'), 1 => array('name' => 'John')))));
    }

    public function testProcessArguments()
    {
        $templatingEngine = new \TemplatingEngine\TemplatingEngine();
        $content = '{{ $foo }}';

        $this->assertEquals('bar', $templatingEngine->process($content, array('foo' => 'bar')));
    }

    public function testProcessArguments2()
    {
        $templatingEngine = new \TemplatingEngine\TemplatingEngine();
        $content = '{{ $bar }}';

        $this->assertEquals('', $templatingEngine->process($content, array('foo' => 'bar')));
    }

    private function _getFilesFolder()
    {
        return __DIR__ . '/files/';
    }

}